$ = jQuery = require('jquery');
var React = require('react');
var ReactDOM = require('react-dom');
var RegistrationForm = require('./users/registrationForm');

ReactDOM.render(<RegistrationForm />, document.getElementById('app'));
