"use strict";

var React = require('react');

class RegistrionForm extends React.Component {

    render() {
        return (
            <form>
                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <input type="text" class="form-control" id="firstName" placeholder="First Name" />
                    <label for="lastName">Last Name</label>
                    <input type="text" class="form-control" id="lastName" placeholder="Last Name" />
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" placeholder="Email Address" />
                    <label for="dateofbirth">Age as of December 31st 2019</label>
                    <input type="datetime" class="form-control" id="dateofbirth" />
                    <label for="gender">Gender</label>
                    <input type="radio" class="form-control" name="gender" value="male" /> Male 
                    <input type="radio" class="form-control" name="gender" value="female" /> Female 
                    <label for="raceTypePreference">Race Length</label>
                    <input type="radio" class="form-control" name="raceTypePreference" value="short" /> Short 3-5 Miles
                    <input type="radio" class="form-control" name="raceTypePreference" value="long" /> Long 7-9 Miles

                </div>
            </form>
        );
    }
}

module.exports = RegistrionForm;
